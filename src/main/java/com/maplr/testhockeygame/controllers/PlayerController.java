package com.maplr.testhockeygame.controllers;

import com.maplr.testhockeygame.dto.PlayerEntityDto;
import com.maplr.testhockeygame.entities.PlayerEntity;
import com.maplr.testhockeygame.mappers.PlayerMapper;
import com.maplr.testhockeygame.services.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;



@RequestMapping("api")
@RestController
public class PlayerController {

    @Autowired
    private PlayerService playerService;

    @Autowired
    private PlayerMapper playerMapper;

    @PutMapping("/player/captain/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<PlayerEntityDto> setCapitain(@PathVariable("id") Long id){
        PlayerEntity player = playerService.setCaptain(id);
        PlayerEntityDto playerEntityDto = playerMapper.entityToDto(player);
        return ResponseEntity.ok(playerEntityDto);
    }

}
