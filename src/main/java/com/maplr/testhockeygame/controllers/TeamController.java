package com.maplr.testhockeygame.controllers;

import com.maplr.testhockeygame.dto.PlayerEntityDto;
import com.maplr.testhockeygame.entities.PlayerEntity;
import com.maplr.testhockeygame.entities.TeamEntity;
import com.maplr.testhockeygame.mappers.PlayerMapper;
import com.maplr.testhockeygame.mappers.TeamMapper;
import com.maplr.testhockeygame.services.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;


import java.util.Optional;

@RequestMapping("api")
@RestController
public class TeamController {

    @Autowired
    private TeamService teamService;

    @Autowired
    private TeamMapper teamMapper;

    @Autowired
    private PlayerMapper playerMapper;

    @GetMapping("/team/{year}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Optional<TeamEntity> getTeamsByYears(@PathVariable("year") Long year){
        return teamService.getTeamByYears(year);
    }

    @PostMapping("/team/{year}")
    @ResponseStatus(HttpStatus.CREATED)
    public TeamEntity savePlayerintoTeam(@RequestBody PlayerEntityDto playerEntityDto, @PathVariable("year") Long year)  {
        PlayerEntity player =  playerMapper.dtoToEntity(playerEntityDto);
        return teamService.savePlayersInTeamYear(player , year);
    }


}
