package com.maplr.testhockeygame.dto;

import com.maplr.testhockeygame.entities.TeamEntity;
import lombok.Getter;
import lombok.Setter;



@Getter
@Setter
public class PlayerEntityDto  {

    private Long id;
    private Long number;
    private String name;
    private String lastname;
    private String position;
    private Boolean isCaptain ;
    private TeamEntity teamEntity;
}
