package com.maplr.testhockeygame.dto;

import com.maplr.testhockeygame.entities.PlayerEntity;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class TeamEntityDto {


    private Long id;
    private String coach;
    private long year;
    private List<PlayerEntity> players;

}
