package com.maplr.testhockeygame.mappers;

import com.maplr.testhockeygame.dto.PlayerEntityDto;
import com.maplr.testhockeygame.entities.PlayerEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PlayerMapper {

    PlayerEntityDto entityToDto(PlayerEntity player);

    PlayerEntity dtoToEntity(PlayerEntityDto playerEntityDto);

    List<PlayerEntityDto> entityToDtoList(List<PlayerEntity> player);

    List<PlayerEntity> dtoToEntityList(List<PlayerEntityDto> playerEntityDto);



}
