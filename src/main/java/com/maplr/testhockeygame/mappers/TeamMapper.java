package com.maplr.testhockeygame.mappers;

import com.maplr.testhockeygame.dto.TeamEntityDto;
import com.maplr.testhockeygame.entities.TeamEntity;
import org.mapstruct.Mapper;

import java.util.Optional;


@Mapper(componentModel = "spring")
public interface TeamMapper {

    TeamEntityDto entityToDto(Optional<TeamEntity> player);

    TeamEntity dtoToEntity(TeamEntityDto teamEntityDto);


}
