package com.maplr.testhockeygame.repositories;

import com.maplr.testhockeygame.entities.PlayerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayerEntityRepository extends JpaRepository<PlayerEntity, Long> {

}
