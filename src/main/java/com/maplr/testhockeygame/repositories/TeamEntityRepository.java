package com.maplr.testhockeygame.repositories;

import com.maplr.testhockeygame.entities.TeamEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface TeamEntityRepository extends JpaRepository<TeamEntity, Long> {
    Optional<TeamEntity> findTeamEntityByYear(Long year);
}
