package com.maplr.testhockeygame.services;

import com.maplr.testhockeygame.entities.PlayerEntity;
import com.maplr.testhockeygame.repositories.PlayerEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.Optional;

@Service
public class PlayerService {

    @Autowired
    private PlayerEntityRepository playerEntityRepository;


    public PlayerEntity setCaptain(Long id) {
        Optional<PlayerEntity> player = playerEntityRepository.findById(id);
            if (player.isPresent()){
                player.get().setCaptain(Boolean.TRUE);
                return playerEntityRepository.save(player.get());
            }else {
                return player.orElse(null);
            }
    }
}
