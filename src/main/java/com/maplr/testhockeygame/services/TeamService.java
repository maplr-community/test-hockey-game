package com.maplr.testhockeygame.services;

import com.maplr.testhockeygame.entities.PlayerEntity;
import com.maplr.testhockeygame.entities.TeamEntity;
import com.maplr.testhockeygame.repositories.TeamEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.Collections;
import java.util.Optional;


@Service
public class TeamService {

    @Autowired
    private TeamEntityRepository teamEntityRepository;


    public Optional<TeamEntity> getTeamByYears(Long year){
        return teamEntityRepository.findTeamEntityByYear(year);
    }

    public TeamEntity savePlayersInTeamYear(PlayerEntity playerEntity , Long year) {

      Optional<TeamEntity> retreiveTeam = teamEntityRepository.findTeamEntityByYear(year);

          if (retreiveTeam.isPresent()){
              retreiveTeam.get().getPlayers().add(playerEntity);
             return teamEntityRepository.save(retreiveTeam.get());
          }else {
              TeamEntity teamEntity = new TeamEntity();
              teamEntity.setYear(year);
              teamEntity.setCoach("Dominique Ducharme");
              teamEntity.setPlayers(Collections.singletonList(playerEntity));
              return teamEntityRepository.save(teamEntity);
          }
    }

}
